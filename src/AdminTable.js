import React, { Component } from 'react';
import {Tooltip} from 'react-lightweight-tooltip';


class AdminTable extends Component {

    render() {

        let datas = this.props.veriler

        return (

<div>
<p> Son 14 günün projelerini goruyorsunuz...</p>

            <table class="order-table table sortable">
                <thead>
                    <tr>
                        <th>PID</th>
                        <th>Birim</th>
                        <th>Proje Kısa Adı</th>
                        <th>Proje Uzun Adı</th>
                        <th>İş Tipi</th>
                        <th>Yaşatan</th>
                        <th>Versiyon</th>
                        <th>Kaynak</th>
                        <th>Proje Yöneticisi</th>
                        <th>Belge Türü</th>
                        <th>B.Uzantısı</th>
                        <th>Bilgilendirme</th>
                        <th>İndir</th>
                        <th>İptal</th>
                        <th>Durum</th>
                        <th>Revizyon</th>
                    </tr>
                </thead>
                <tbody>
                    {datas.yeniDosya.map((file, i) => {
                        return <tr key={i}>
                            <td>{file.PID}</td>
                            <td>

                                {file.PID != 'yok' || file.yayinDurumu == "POTA'da" || file.yayinDurumu == "Revizyon" ? file.birim :
                                    <div>
                                        <input defaultValue={file.birim} size="25" list="birimler" type="text" name="birim" className={file.id} id={file.id + "" + 0} />
                                        <datalist id="birimler">
                                            {
                                                datas.birimler.map(obj => {
                                                    return <option value={obj}>{obj}</option>
                                                })
                                            }

                                        </datalist>

                                    </div>
                                }
                            </td>
                            <td>

                                {file.PID != 'yok' || file.yayinDurumu == "POTA'da" || file.yayinDurumu == "Revizyon" ? file.projeKisaAdi : <input defaultValue={file.projeKisaAdi} size="25" name="projeKisaAdi" type="text" className={file.id} id={file.id + "" + 1} />
                                }
                            </td>
                            <td>
                                {file.PID != 'yok' || file.yayinDurumu == "POTA'da" || file.yayinDurumu == "Revizyon"  ? file.projeUzunAdi :
                                    <input defaultValue={file.projeUzunAdi} name="projeUzunAdi" type="text" className={file.id} id={file.id + "" + 2} />
                                }
                            </td>
                            <td>
                                {file.PID != 'yok' || file.yayinDurumu == "POTA'da" || file.yayinDurumu == "Revizyon" ? file.isTipi :
                                    <div>
                                        <input defaultValue={file.isTipi} size="8" type="text" name="isTipi" list="istipleri" className={file.id} id={file.id + "" + 3} />
                                        <datalist id="istipleri">
                                            {
                                                datas.istipleri.map(obj => {
                                                    return <option value={obj}>{obj}</option>
                                                })
                                            }

                                        </datalist> </div>
                                }

                            </td>
                            <td>
                                {file.PID != 'yok' || file.yayinDurumu == "POTA'da" || file.yayinDurumu == "Revizyon" ? file.yasatan :
                                    <input defaultValue={file.yasatan} size="5" name="yasatan" type="text" className={file.id} id={file.id + "" + 4} />
                                }
                            </td>
                            <td>
                                {file.PID != 'yok' || file.yayinDurumu == "POTA'da" || file.yayinDurumu == "Revizyon" ? file.versiyon :
                                    <input defaultValue={file.versiyon} size="5" name="versiyon" type="text" placeholder="v1.0" className={file.id} id={file.id + "" + 5} />
                                }
                            </td>
                            <td>
                                {file.PID != 'yok' || file.yayinDurumu == "POTA'da" || file.yayinDurumu == "Revizyon"  ? file.kaynak :

                                    <select name="kaynak" value={file.kaynak} className={file.id} id={file.id + "" + 6} required>
                                        <option value="İçKaynak">İçkaynak</option>
                                        <option value="DışKaynak" >Dışkaynak</option>
                                        <option value="İçDış">İçDış</option>

                                    </select>
                                }
                            </td>
                            <td>
                                {file.PID != 'yok' || file.yayinDurumu == "POTA'da" || file.yayinDurumu == "Revizyon" ? file.yonetici :
                                    <div>
                                        <input defaultValue={file.yonetici} type="text" name="yonetici" list="yoneticiler" className={file.id} id={file.id + "" + 7} />
                                        <datalist id="yoneticiler">
                                            {
                                                datas.yoneticiler.map(obj => {
                                                    return <option value={obj}>{obj}</option>
                                                })
                                            }

                                        </datalist>
                                    </div>
                                }

                            </td>
                            <td>
                                {file.PID != 'yok' || file.yayinDurumu == "POTA'da" || file.yayinDurumu == "Revizyon"  ? file.belgeTipi || file.belgeTipi1 :

                                    <div>
                                        <input defaultValue={file.belgeTipi || file.belgeTipi1} size="30" type="text" name="belgeTipi" list="belgetipleri" className={file.id} id={file.id + "" + 8} />
                                        <datalist id="belgetipleri">
                                            {
                                                datas.belgetipleri.map(obj => {
                                                    return <option value={obj}>{obj}</option>
                                                })
                                            }

                                        </datalist>
                                    </div>
                                }
                            </td>
                            <td>{file.belgeUzantisi}</td>
                            <td className={file.belgeVarligi === "Belge Var" ? "belge-var" : "belge-yok"}>{file.belgeVarligi}</td>
                            <td>
                            {
                                    file.dosya ?

                                    <a href={file.dosya} download={file.id + "." + file.belgeUzantisi}>indir</a>
                                    :
                                    <a href={this.props.host+"upload/indir/"+file.id} > indir </a>
                                }

                            </td>
                            <td> <a href="" id={file.id} onClick={this.props.handleSil}>{file.silDurumu}</a> </td>
                            <td className="Onay-Durumu" >

                                {file.revizyonSebebi ?

                                    <Tooltip content={file.revizyonSebebi}>
                                        <a href="#" id={file.id} onClick={this.props.handleSubmit} >
                                            {file.yayinDurumu}
                                        </a>
                                    </Tooltip>
                                    :
                                    <a href="#" id={file.id} onClick={this.props.handleSubmit} >
                                        {file.yayinDurumu}
                                    </a>
                                }
                            </td>
                            <td>


                               {
                                   file.yayinDurumu == 'Yayınla' ? 
                                     <a href="" id={file.id} onClick={this.props.handleRevizyon}>Revizyon</a>
                                   : 
                                    ""
                               } 
                            </td>
                        </tr>
                    })}

                </tbody>
            </table>
            </div>
        )
    }
}


export default AdminTable