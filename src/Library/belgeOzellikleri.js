// dosya yolu verilen word dosyasının, belge ozelliklerini object olarak return ediyor. sadece docx uzantili dosyalari okur
// Parameter: string dosya yolu
// return : obj dosya ozellikleri
function getBelgeOzellikleri(strFile) {

    // word dosyalarinin properties'larini okumaya yarayan kutuphane
    return new Promise((resolve, reject) => {

        let getDocumentProps = require('office-document-properties')
        // Read document properties from file path.
        getDocumentProps.fromBuffer(strFile, function (err, data) {
            if (err) throw err;
           // console.log(data)
            resolve(data)
        })
    })
}

module.exports = getBelgeOzellikleri