import React, { Component } from 'react';
import {Tooltip} from 'react-lightweight-tooltip';

class MisafirTable extends Component {



    render() {

        let datas = this.props.veriler

        return (<div>
            <p> Son 8 günün projelerini goruyorsunuz...</p>
            <table class="order-table table sortable">
                <thead>
                    <tr>
                        <th>PID</th>
                        <th>Birim</th>
                        <th>Proje Kısa Adı</th>
                        <th>Proje Uzun Adı</th>
                        <th>İş Tipi</th>
                        <th>Yaşatan</th>
                        <th>Versiyon</th>
                        <th>Kaynak</th>
                        <th>Proje Yöneticisi</th>
                        <th>Belge Türü</th>
                        <th>B.Uzantısı</th>
                        <th>Bilgilendirme</th>
                        <th>İndir</th>
                        <th>İptal</th>
                        <th>Durum</th>
                    </tr>
                </thead>
                <tbody>
                    {datas.yeniDosya.map((file, i) => {
                        return <tr key={i}>
                            <td>{file.PID}</td>
                            <td>

                                {file.birim || <div>
                                    <input type="text" name="birim" list="birimler" className={file.id} id={file.id + "" + 0} />
                                    <datalist id="birimler">
                                        {
                                            datas.birimler.map(obj => {
                                                return <option value={obj}>{obj}</option>
                                            })
                                        }

                                    </datalist> </div>
                                }
                            </td>
                            <td>{file.projeKisaAdi || <input defaultValue={file.projeKisaAdi} name="projeKisaAdi" type="text" className={file.id} id={file.id + "" + 1} />}</td>
                            <td>{file.yayinDurumu != "Gönder" || file.PID != "yok" ? file.projeUzunAdi : <input defaultValue={file.projeUzunAdi} name="projeUzunAdi" type="text" className={file.id} id={file.id + "" + 2} />} </td>
                            <td>
                                {file.isTipi || <div>
                                    <input defaultValue="" type="text" name="isTipi" list="istipleri" className={file.id} id={file.id + "" + 3} />
                                    <datalist id="istipleri">
                                        {
                                            datas.istipleri.map(obj => {
                                                return <option value={obj}>{obj}</option>
                                            })
                                        }

                                    </datalist> </div>
                                }

                            </td>
                            <td>{file.yasatan || <input defaultValue="" name="yasatan" type="text" className={file.id} id={file.id + "" + 4} />}</td>
                            <td>{file.versiyon || <input defaultValue="" name="versiyon" type="text" placeholder="v1.0" className={file.id} id={file.id + "" + 5} />}</td>
                            <td>
                                {file.kaynak ||
                                    <select name="kaynak" className={file.id} id={file.id + "" + 6} >
                                        <option value=""></option>
                                        <option value="İçKaynak">İçkaynak</option>
                                        <option value="Dışkaynak">Dışkaynak</option>
                                        <option value="İçDış">İçDış</option>

                                    </select>
                                }
                            </td>
                            <td>

                                {file.yonetici || <div>
                                    <input defaultValue="" type="text" name="yonetici" list="yoneticiler" className={file.id} id={file.id + "" + 7} />
                                    <datalist id="yoneticiler">
                                        {
                                            datas.yoneticiler.map(obj => {
                                                return <option value={obj}>{obj}</option>
                                            })
                                        }

                                    </datalist>
                                </div>
                                }

                            </td>
                            <td>
                                {file.belgeTipi || file.belgeTipi1 || <div>
                                    <input defaultValue="" type="text" name="belgeTipi" list="belgetipleri" className={file.id} id={file.id + "" + 8} />
                                    <datalist id="belgetipleri">
                                        {
                                            datas.belgetipleri.map(obj => {
                                                return <option value={obj}>{obj}</option>
                                            })
                                        }

                                    </datalist>
                                </div>
                                }
                            </td>
                            <td>{file.belgeUzantisi}</td>
                            <td className={file.belgeVarligi === "Belge Var" ? "belge-var" : "belge-yok"}>{file.belgeVarligi}</td>
                            <td>
                                {
                                    file.dosya ?

                                    <a href={file.dosya} download={file.id + "." + file.belgeUzantisi}>indir</a>
                                    :
                                    <a href={this.props.host+"upload/indir/"+file.id} > indir </a>
                                }
                            </td>
                            <td>

                                <a href="" id={file.id} onClick={this.props.handleSil}>{file.silDurumu}</a>
                            
                            </td>
                            <td className="Onay-Durumu" style={file.revizyonSebebi ? {backgroundColor:'yellow'} : {}}>
                               { file.revizyonSebebi ? 
                               
                               <Tooltip content={file.revizyonSebebi}>
                                    <a href="#" id={file.id} onClick={this.props.handleSubmit} >
                                        {file.yayinDurumu}
                                    </a>
                                </Tooltip>
                                : 
                                <a href="#" id={file.id} onClick={this.props.handleSubmit} >
                                    {file.yayinDurumu}
                                </a>
                               }
                            </td>
                        </tr>
                    })}

                </tbody>
            </table>
</div>
        )
    }
}
export default MisafirTable

