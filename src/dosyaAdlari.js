
function setDosyaAdiniAl(str) {
    let lastIndex = str.lastIndexOf(".")
    return str.slice(0, lastIndex)
}

// array olarak giren dosya isimini, json olarak return ediyor.
// tum dosya adlarini json olarak return ediyor.
// return array object, json [{}, {}]
function getDosyaAdlariJSON(dosyaAdi) {
    return new Promise((resolve, reject) => {

        // değişkenleri her seferinde sıfırlıyoruz
        let arrDosyaAdi = []
        let strDosyaAdi = ""
        let intSonSlashIndex = -1
        let strProjeAdi, strProjeKisaAdi, strProjeUzunAdi, strSeflikVeAdlari, strProgram = ""
        let objDosyaAdi = {}

        // strItem degiskeni string olarak tüm dosya ismini path ile birlikte iceriyor
        intSonSlashIndex = dosyaAdi.lastIndexOf("/") // proje dosya adinin basladigi yerin tespiti
        strDosyaAdi = dosyaAdi.slice(intSonSlashIndex + 1) // proje dosya adini kesip alıyoruz
        arrDosyaAdi = strDosyaAdi.split(/_/) // string verinin parcalanip return array olarak çıktı alınması
        strProjeAdi = arrDosyaAdi[0] // arraydeki ilk veri projenin kisa ve uzun adini iceriyor.
        strSeflikVeAdlari = dosyaAdi.slice(0, intSonSlashIndex) // projenin bagli oldugu birimleri kesip aliyoruz 
        //strSeflikVeAdlari = seflik // okunacak dosya yolunu seflik ve amir adlarindan siliyoruz
// console.log("arrdosyaadi"+arrDosyaAdi)
        // birimler klasorlerden oluştuğu için, içerisinde program kelimesi geçiyor mu diye kontrol ediyoruz.
        // program kelimesini içeriyorsa, onu da json verisine ekliyoruz.
        let intSonSlashIndex2 = strSeflikVeAdlari.lastIndexOf("/")
        if (strSeflikVeAdlari.slice(intSonSlashIndex2 + 1).includes("Program")) {
            strProgram = strSeflikVeAdlari.slice(intSonSlashIndex2 + 1)

            // program klasorunu birim içerisine dahil etmedigimiz icin, programi birim metinden cikariyoruz.
            strSeflikVeAdlari = strSeflikVeAdlari.slice(0, intSonSlashIndex2)
        }

        // dosya ağda veya başka biryerde olacağı için, yukarıda verdiğimiz adres yolunu metinden siliyoruz.
        // dosya etiketleri sayisi 7 tane olmak zorunda. Yoksa loglara yaziliyor yazilamayan bilgiler.
        if (arrDosyaAdi.length == 7) {

            // proje adinda parantez var ise, ayrıştırıp uzun ve kısa adını alma
            if (arrDosyaAdi[0].endsWith(")")) {
                arrDosyaAdi[0] = arrDosyaAdi[0].trim()
                arrDosyaAdi[0] = arrDosyaAdi[0].slice(0, -1)
                arrDosyaAdi[0] = arrDosyaAdi[0].split(/\(/)
                strProjeKisaAdi = arrDosyaAdi[0][0]
                strProjeUzunAdi = arrDosyaAdi[0][1]
            }
            else {
                strProjeKisaAdi = arrDosyaAdi[0]
                strProjeUzunAdi = ""
            }
            // console.log(arrDosyaAdi)

            // parçaladığımız proje bilgilerini nesne içerisine atma
            objDosyaAdi = {
                PID: "yok",
                birim: strSeflikVeAdlari.trim(),
                program: strProgram.trim(),
                projeKisaAdi: strProjeKisaAdi.trim(),
                projeUzunAdi: strProjeUzunAdi.trim(),
                projeAdi: (strProjeKisaAdi+" "+strProjeUzunAdi).trim(),
                isTipi: arrDosyaAdi[1].trim(),
                yasatan: arrDosyaAdi[2].trim(),
                versiyon: arrDosyaAdi[3].trim(),
                kaynak: arrDosyaAdi[4].trim(),
                yonetici: arrDosyaAdi[5].trim(),
                belgeTipi: setDosyaAdiniAl(arrDosyaAdi[6].replace(/\(\d+\)|\d+/,"")).trim(), // dosya uzantisi belge turunden siliyoruz. Orn: Proje Kapanış Belgesi.docx -> Proje Kapanış Belgesi
                belgeUzantisi: arrDosyaAdi[6].split(".")[1].trim(),
                dosyaYolu: dosyaAdi.replace("_" + arrDosyaAdi[7], "").trim(),
                //dosyaGuncTar: arrDosyaAdi[7], // tarihi son dosya _ ayrimi olarak yazdik ve burada ayiriyoruz. klasordeki dosyalar okunurken yukarida getDosyaAdlari'nda ekledik.
                dosyaID: arrDosyaAdi[6].match(/\d+/g) == null ? 1 : parseInt(arrDosyaAdi[6].match(/\d+/g)[0].trim())  // belge adindaki son sayiyi aliyoruz 
            }

            // resim olanlarin belge tipini her zaman icin Proje Resim Belgesi olarak adlandiriyoruz.
            if(dosyaAdi.toLowerCase().endsWith("jpeg") || dosyaAdi.toLowerCase().endsWith("jpg") || dosyaAdi.toLowerCase().endsWith("png")) {
                objDosyaAdi.belgeTipi = "Proje Resim Belgesi"
            }

            resolve(objDosyaAdi) // /-|\/|\(|\)/ ayrıştırma işlemini (, ), -, / a göre ayristirma yapiyor regex kullanılarak.

        }
        else if (arrDosyaAdi.length == 8 && !isNaN(arrDosyaAdi[0])) { // dosya adı 8 etiketli ve ilk etiket numara ise

            // proje adinda parantez var ise, ayrıştırıp uzun ve kısa adını alma
            if (arrDosyaAdi[1].endsWith(")")) {
                arrDosyaAdi[1] = arrDosyaAdi[1].trim()
                arrDosyaAdi[1] = arrDosyaAdi[1].slice(0, -1)
                arrDosyaAdi[1] = arrDosyaAdi[1].split(/\(/)
                strProjeKisaAdi = arrDosyaAdi[1][0]
                strProjeUzunAdi = arrDosyaAdi[1][1]
            }
            else {
                strProjeKisaAdi = arrDosyaAdi[1]
                strProjeUzunAdi = ""
            }
            // console.log(arrDosyaAdi)

            // parçaladığımız proje bilgilerini nesne içerisine atma
            objDosyaAdi = {
                PID: arrDosyaAdi[0],
                birim: strSeflikVeAdlari,
                program: strProgram,

                projeKisaAdi: strProjeKisaAdi.trim(),
                projeUzunAdi: strProjeUzunAdi.trim(),
                projeAdi: strProjeKisaAdi+" "+strProjeUzunAdi,
                isTipi: arrDosyaAdi[2].trim(),
                yasatan: arrDosyaAdi[3].trim(),
                versiyon: arrDosyaAdi[4].trim(),
                kaynak: arrDosyaAdi[5].trim(),
                yonetici: arrDosyaAdi[6],
                belgeTipi: setDosyaAdiniAl(arrDosyaAdi[7].replace(/\d+/,"")).trim(), // dosya uzantisi belge turunden siliyoruz. Orn: Proje Kapanış Belgesi.docx -> Proje Kapanış Belgesi
                belgeUzantisi: arrDosyaAdi[7].split(".")[1].trim(),
                dosyaYolu: dosyaAdi.replace("_" + arrDosyaAdi[8], "").trim(),
               // dosyaGuncTar: arrDosyaAdi[8], // tarihi son dosya _ ayrimi olarak yazdik ve burada ayiriyoruz. klasordeki dosyalar okunurken yukarida getDosyaAdlari'nda ekledik.
                dosyaID: arrDosyaAdi[7].match(/\d+/g) == null ? 1 : parseInt(arrDosyaAdi[7].match(/\d+/g)[0].trim())  // belge adindaki son sayiyi aliyoruz 
            }

            // resim olanlarin belge tipini her zaman icin Proje Resim Belgesi olarak adlandiriyoruz.
            if(dosyaAdi.toLowerCase().endsWith("jpeg") || dosyaAdi.toLowerCase().endsWith("jpg") || dosyaAdi.toLowerCase().endsWith("png")) {
                objDosyaAdi.belgeTipi = "Proje Resim Belgesi"
            }

            resolve(objDosyaAdi) // /-|\/|\(|\)/ ayrıştırma işlemini (, ), -, / a göre ayristirma yapiyor regex kullanılarak.

        }
        else {
            let hatalar = dosyaAdi + "\n"
        }



    })

}


module.exports = getDosyaAdlariJSON