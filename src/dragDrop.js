import React, { Component } from 'react';
import logo from './logo.svg';
import Files from "react-butterfiles";
import './dragDrop.css';
import belgeOzellikleri from './Library/belgeOzellikleri'
import dosyaAdlari from './dosyaAdlari'
import AdminTable from './AdminTable'
import MisafirTable from './MisafirTable'


// popup modal pencere
import Drawer from 'react-drag-drawer'


// loading gorseli
import { css } from '@emotion/core';
// First way to import
import { DotLoader } from 'react-spinners';
let ids = require('short-id'); //unique id olusturmak icin kullaniyoruz.

const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
`;
//import { isPromiseAlike } from 'q';

let host = "http://cbshozdasci/bidb/";
let yeniDosya = []

class App extends Component {

    constructor(props) {

        super(props)
        this.state = {
            files: [],
            errors: [],
            yeniDosya: [],
            birimler: [],
            istipleri: [],
            yoneticiler: [],
            belgetipleri: [],
            loading: false,
            onayDurumu: "Gönder",
            yayinDurumu: "Beklemede",
            pswrd: "",
            toggle: false,
            toggle2: false,
            kullanici: "Misafir"
        }

        this.handleSil = this.handleSil.bind(this);

        this.handleSubmit = this.handleSubmit.bind(this);
        this.toggle = this.toggle.bind(this);
        this.toggle2 = this.toggle2.bind(this);

        this.handleAdminDosyalari = this.handleAdminDosyalari.bind(this);
        this.handleRevizyon = this.handleRevizyon.bind(this);

    }

    // örnekler icin popup pencereyi tetikliyor.
    toggle() {
        let { toggle } = this.state

        this.setState({ toggle: !toggle })
    }

    // password için popup pencereyi tetikliyor
    toggle2() {
        let { toggle2 } = this.state

        if (toggle2) {
            document.getElementById("password").focus()
        }
        this.setState({ toggle2: !toggle2 })
 
    }

    /*
        handleChange(event) {
    
            // input değşimlerinin değerlerini yakalıyoruz.
            yeniDosya = this.state.yeniDosya.map(i => {
                if (i.ID == event.target.id) {
                    let tip = event.target.name
                    i[tip] = event.target.value
                    return i
                }
                else return i
            })
            //this.setState({yeniDosya})
            console.log(yeniDosya)
        }*/

    handleSil(event) {
        event.preventDefault();
        let yeniDosya = this.state.yeniDosya.filter(i => {
            //  console.log(event.target.id != event.target.id)
            return i.id != event.target.id
        })
        this.setState({ yeniDosya })
    }

    // gonder butonuna basildiginda put islemi ile sunucuya yaz
    // yayınla butonuna basildiginda put islemi ile sunucuya yaz
    handleSubmit(event) {
        event.preventDefault();
        let durum = event.target.innerHTML.trim()

        // yayınlandı ve beklemede gibi butonlar calismayacak.
        if (durum === "Yayınlandı" || durum === "Beklemede" || durum === "Yüklendi" || durum === "POTA'da" || durum === "Revizyon") {
            return;
        }

        // data.append('file', this.state.yeniDosya[0].src.base64)
        let bosVar = false;
        let eventID = event.target.id
        let inputs = document.getElementsByClassName(eventID) // ilgili id'deki tüm inputlar aliniyor

        // bos input var mi diye kontrol ediyoruz. bosluk varsa submit engelleniyor.
        Array.from(inputs).forEach(i => {

            if (i.value.trim() == "" && i.name != "projeUzunAdi") { // bos input var ise
                i.style.backgroundColor = "khaki"
                i.focus()
                bosVar = true;
            }
            else {

                // inputlar dolduruldu ise, input değşimlerinin değerlerini yakalıyoruz.
                // sorun yoksa varolanlari da dahil edip return ediyor
                yeniDosya = this.state.yeniDosya.map(k => {
                    if (k.id == eventID) {
                        k[i.name] = i.value
                        return k
                    }
                    else return k
                })
                //  console.log(yeniDosya)
                this.setState({ yeniDosya })
            }
        })

        // form inputlar tamamen doldurulduysa, sunucuya dosya gönderiliyor.
        if (bosVar) {
            alert("Lütfen zorunlu alanları doldurun")
        } else {

            // header olusturma post icin
            // password gonderme alanı
            let headers = new Headers();
            let password = this.state.pswrd // admin butonuna basinca state yazilan sifre , aslinda sifreleyip gondermekte fayda var.
            //  headers.append('Authorization', 'Basic ' + password);
            // headers.append('Authorization', password);
            headers.append('Content-Type', 'application/json')

            let ClientID = this.ClientID() // client bilgisayara atanan bir unique id

            let data = this.state.yeniDosya.find(i => i.id == eventID) // tek bir obje donuyor.
            Object.assign(data, { ClientID })

            let body = {}
            body.data = data
            body.adminPassword = password
            body.tip = 'handleGonder'

            // data = JSON.stringify(data)
            // console.log("bulunan data"+data)
            fetch(host + 'projeler', {
                method: 'put',
                body: JSON.stringify(body),
                headers: headers
            })
                .then(x => x.json())
                .catch(error => console.error("hata:" + error))
                .then(x => {

                    Object.assign(data, x)

                    let yeniDosya = this.state.yeniDosya.map(k => k.id == data.id ? Object.assign(k, x) : k)

                    this.setState({ yeniDosya })

                })
        }

        /*
        // verileri yenidosya degiskenine global olarak yeniden yaziyoruz, formlardaki herhangi bir degisiklikte.
        let newState;
        let elm = []
        if (yeniDosya.length > 0) {
            newState = yeniDosya.find(i => {
                return i.ID == event.target.id
            })
        }
        else {

            newState = this.state.yeniDosya.find(i => {
 
                return i.ID == event.target.id
            })
        }
        let eksikler = this.formEksikligiKontrol(newState) */
        //  console.log(eksikler)
        //  elm = document.getElementsByClassName(event.target.id)
        // console.log(elm[0])
        /* let hehe = elm[0]
         console.log(hehe.name)
         for(let i = 0; i<elm.length; i++) {
             eksikler.find(x=>x ===  elm[i].name ? console.log("bulduk"): console.log("bulamadik"))
             // console.log(elm[i].name)
         } */
        //let snc1 = elm.find(x=>x.name === "isTipi")
        // snc.style.backgroundColor = "red"
        // console.log(snc1)
        /*
        fetch(host + 'projeler/665&Proje%20Fikir%20Belgesi')
            .then(x => x.json())
            //    .then(x => console.log(JSON.stringify(x)))
            .catch(error => console.error("hata:" + error)) */
    }

    // admin tarafindan revizyon edilmesi istenilen dosyalar icin bir event
    handleRevizyon(event) {
        event.preventDefault()
        let eventID = event.target.id

        let sebep = prompt("Revizyon sebebini yazar mısınız?", "");
        let obj = { yayinDurumu: "Revizyon", revizyonSebebi: sebep, id: eventID }
      //  console.log(obj)

        // header olusturma post icin
        // password gonderme alanı
        let headers = new Headers();
        let password = this.state.pswrd // admin butonuna basinca state yazilan sifre , aslinda sifreleyip gondermekte fayda var.
        //  headers.append('Authorization', 'Basic ' + password);
        // headers.append('Authorization', password);
        headers.append('Content-Type', 'application/json')

        //     let ClientID = this.ClientID() // client bilgisayara atanan bir unique id

        //   let data = this.state.yeniDosya.find(i => i.id == eventID) // tek bir obje donuyor.
        //       Object.assign(data, { ClientID })

        let body = {}
        //          body.data = data
        body.adminPassword = password
        body.data = obj
        body.tip = "handleRevizyon"

        // data = JSON.stringify(data)
        // console.log("bulunan data"+data)
        fetch(host + 'projeler', {
            method: 'put',
            body: JSON.stringify(body),
            headers: headers
        })
            .then(x => x.json())
            .catch(error => console.error("hata:" + error))
            .then(x => {

                let yeniDosya = this.state.yeniDosya.map(k => k.id == x.id ? Object.assign(k, x) : k)

                this.setState({ yeniDosya })

            })


    }

    // kullanici bilgisayarina atilan bir uniq numara ile, kullanıcının sisteme attığı son 8 gün upload listesini gostermeyi saglamaktadir.
    ClientID() {
        let ClientID = localStorage.getItem('potaClientID');
        if (ClientID == null) {
            ClientID = ids.generate(); // uniq id olusturma
            localStorage.setItem('potaClientID', ClientID);
        }
        return ClientID
    }

    // kullanmiyorum suan icin. gonder butonuna basilinca kullanicinin doldurdugu formun dogrulugunu kontrol ediyoruz.
    formEksikligiKontrol(obj) {
        let durum = []
        Object.keys(obj).map(i => {
            if (obj[i] == "" || obj[i] == undefined) {
                durum.push(i)
            }
        })
        return durum
    }

    // drag drop yapinca, kullanici dosyalari sunucudan gerekli alanlari cekiyor.
    // isim standartlari upload edilince uydugunda sunucuya request yapiliyor.
    sunucuSorgula() {
        let sonuc = this.state.yeniDosya.map(obj => {

            // console.log("bakalim:" + JSON.stringify(obj))
            let { PID, projeAdi, projeKisaAdi, projeUzunAdi, isTipi, yasatan, versiyon, kaynak, yonetici, dosyaID, belgeTipi, yayinDurumu } = obj

            projeAdi = (projeKisaAdi.trim() + (projeUzunAdi ? " (" + projeUzunAdi.trim() + ")" : "")).trim()
            belgeTipi = obj.belgeTipi || obj.belgeTipi1 || ""

            let data = { PID, projeAdi, projeKisaAdi, projeUzunAdi, isTipi, yasatan, versiyon, kaynak, yonetici, dosyaID, belgeTipi, yayinDurumu }


            return fetch(host + "projeler", {
                method: 'POST',
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
                .then(x => x.json())
                .catch(error => console.error("hata: " + error))
                .then(x => {
                    return Object.assign(obj, x)
                })
        })

        Promise.all(sonuc)
            .then(yeniDosya => this.setState({ yeniDosya, loading: false }))

        // birim listeleri servisten guruplanarak aliniyor ve state yaziliyor
        fetch(host + "birimler")
            .then(x => x.json())
            .catch(error => console.error("hata: " + error))
            .then((birimler) => this.setState({ birimler }))

        // istipleri cekiyoruz
        fetch(host + "istipleri")
            .then(x => x.json())
            .catch(error => console.error("hata: " + error))
            .then((istipleri) => this.setState({ istipleri }))

        // proje yoneticileri cekiyoruz
        fetch(host + "yoneticiler")
            .then(x => x.json())
            .catch(error => console.error("hata: " + error))
            .then((yoneticiler) => this.setState({ yoneticiler }))

        // belgetipleri (proje tanım belgesi vs..) cekiyoruz
        fetch(host + "belgetipleri")
            .then(x => x.json())
            .catch(error => console.error("hata: " + error))
            .then((belgetipleri) => this.setState({ belgetipleri }))
    }

    // base64 olarak gelen veri onunde belge tipi ve base64 yazisini ayristirip, raw veriyi ve contenttype return ediyoruz.    
    base64Ayristir(base64) {
        let docxFile = base64
        // Split the base64 string in data and contentType
        let block = docxFile.split(";");
        // Get the content type of the file
        let contentType = block[0].split(":")[1];// In this case "image/gif"
        // get the real base64 content of the file
        let realData = block[1].split(",")[1];// In this case "R0lGODlhPQBEAPeoAJosM...."
        return { contentType, realData }
    }

    blob2file(blobData) {

        const fd = new FormData();
        fd.set('a', blobData);
        return Promise.resolve(fd.get('a'))
    }

    // dosya uzantilarini siler. ve dosya adini donderir Orn: .docx, .jpg vs. uzantili dosyalarin sadece adini alir.
    // parameter: string olarak dosya adi
    // return: uzantisiz dosya adı.
    setDosyaAdiniAl(str) {
        let lastIndex = str.lastIndexOf(".")
        return str.slice(0, lastIndex)
    }

    // teslim edilen dosya adini ayristirip obj return ediyor
    // sadece dosya isimlendirmesi üzerinde islem yapiyor.
    // Ornek: AYBİS Ruhsat (Altyapı Bilgi Sistemi)_Yaz Gel_AKM_w1.0_İçkaynak_Ali USLUCAN_Proje Sorumluluk Belgesi -> arraye donusturur.
    dosyaAdiAyristir(dosyaAdi) {
        return new Promise((resolve, reject) => {

            let obj = {}
            let isim = dosyaAdi.split("_")

            if (dosyaAdi.toLowerCase().endsWith("- kopya.docx")
                || dosyaAdi.toLowerCase().endsWith(".doc")
                || dosyaAdi.toLowerCase().endsWith("resim belgesi.docx")
                || dosyaAdi.toLowerCase().endsWith("- kopya.xlsx")
                || dosyaAdi.toLowerCase().endsWith(".xls")
                || dosyaAdi.toLowerCase().endsWith("resim belgesi.xlsx")
            ) {
                let arr = this.state.errors
                arr.push(dosyaAdi) //"DOSYA ADLANDIRMA HATALI:" 
                this.setState({ errors: arr })
                resolve(false)
            }

            // Ör: Egitim Yönetim Sistemi_Yaz Gel_EDB_v1.0_Outsource_PMO_Proje Değişiklik Belgesi2.docx
            else if (isim.length == 7) {
                obj = dosyaAdlari("/" + dosyaAdi)
            }

            // Ör: 232_Arıza Yönetimi Uygulaması_Yaz Gel_ESM_w1.0_İçkaynak_Pervin İŞCAN_Proje Performans Belgesi8.docx
            else if (isim.length == 8) {
                obj = dosyaAdlari("/" + dosyaAdi) // birim icin dosya basina birimadi/ seklinde yazabiliriz.
            }

            else if (isim.length == 2) {

                // Ör: 10206_Proje Performans Belgesi9.docx
                isim[1] = isim[1].replace(/[0-9]/g, '');
                if (isim[1].endsWith("elgesi.docx") || isim[1].endsWith("elgesi.xlsx")) {
                    let isim2 = isim[0].trim()
                    if (isim2.endsWith(")")) {
                        obj.PID = "yok"
                        let isimArr = isim2.slice(0, -1).split("(")
                        obj.projeKisaAdi = isimArr[0]
                        obj.projeUzunAdi = isimArr[1]
                    } else {
                        obj.PID = isim[0]
                    }

                    let belge = isim[1].split(".")
                    obj.belgeTipi = belge[0]
                    obj.belgeUzantisi = belge[1]
                    // resolve(obj)
                }
                else { // Ör: 10206_AYDOS.docx
                    obj.PID = isim[0]
                    let belge = isim[1].split(".")
                    obj.belgeUzantisi = belge[1]

                    let projeAdi = belge[0].endsWith(")") ? belge[0].slice(0, -1).split("(") : belge[0]
                    obj.projeKisaAdi = Array.isArray(projeAdi) ? projeAdi[0] : projeAdi
                    obj.projeUzunAdi = Array.isArray(projeAdi) ? projeAdi[1] : ""
                    //  resolve(obj)
                }

            }
            // let stringveri = "Selamlar ece"
            // let arrayveri = ["Hakan", "ece", "Esra Sümeyye", "Veysel"]

            // Örnek: 10206_AYDOS (Uzun Proje Adı)_Proje Paydaş Belgesi.docx
            else if (isim.length == 3) {
                //isim = dosyaAdi.split(".")
                obj.PID = isNaN(isim[0]) ? "" : isim[0]
                let projeAdi = isim[1].slice(0, -1).split("(")
                obj.projeKisaAdi = projeAdi[0]
                obj.projeUzunAdi = projeAdi[1]
                let belge = isim[2].split(".")

                obj.belgeTipi = belge[0]
                obj.belgeUzantisi = belge[1]
            }

            // Ör: DENEM (Deneme Projesi).docx
            // Ör: Deneme Projesi.docx
            // Ör: 10206 (3).jpg
            else if (!dosyaAdi.includes("_")) {

                isim = dosyaAdi.split(".")

                if (!dosyaAdi.includes("(")) {  // Ör: Deneme Projesi.docx veya 1211.docx
                    if (isNaN(isim[0])) {
                        obj.PID = "yok"
                        obj.projeKisaAdi = isim[0]
                    }
                    else obj.PID = isim[0]

                } else {

                    let projeAdi = isim[0].includes("(") ? isim[0].trim().slice(0, -1).split("(") : isim[0]
                    if (projeAdi[1].length < 3) { // Ör: 10206 (3).jpg
                        obj.PID = projeAdi[0]

                    } else { // Ör: DENEM (Deneme Projesi).docx
                        obj.PID = isNaN(isim[0]) ? "yok" : isim[0]
                        obj.projeKisaAdi = Array.isArray(projeAdi) ? projeAdi[0] : projeAdi //isNaN(isim[0]) ? isim[0] : ""
                        obj.projeUzunAdi = projeAdi[1]
                    }
                }

                obj.belgeUzantisi = isim[1]
            }

            else {
                let arr = this.state.errors
                arr.push(dosyaAdi) // "DOSYA ADLANDIRMA HATALI:"
                this.setState({ errors: arr })
                resolve(false)
            }

            // resim olanlarin belge tipini her zaman icin Proje Resim Belgesi olarak adlandiriyoruz.
            if (dosyaAdi.toLowerCase().endsWith("jpeg") || dosyaAdi.toLowerCase().endsWith("jpg") || dosyaAdi.toLowerCase().endsWith("png")) {
                obj.belgeTipi = "Proje Resim Belgesi"
            }
            resolve(obj)

        })

    }


    // sayfa ilk cagirildiginda, kullanicinin son 48 saatte gonderdigi dosyalarin listesini return ediyoruz.
    clientDosyalari() {

        let ClientID = this.ClientID()

        fetch(host + 'projeler', {
            method: 'options',
            body: JSON.stringify({ ClientID }),
            headers: {
                'Content-Type': 'application/json'
                // 'Access-Control-Request-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
            }
        })
            .then(x => {
                this.setState({ kullanici: x.headers.get("kullanici") })
                return x.json()
            })
            .catch(error => console.error("hata:" + error))
            .then(yeniDosya => {
                //   console.log("bakalim "+yeniDosya)
                if (Object.keys(yeniDosya).length > 0) {
                    this.setState({ yeniDosya })
                }

            })
    }

    // drag drop ile sayfaya atılan dosyalarin
    // office dokumanlarının ozelliklerini aliyoruz. 
    // dosya isimlerini okuyup adlarini ayristiriyoruz
    // sonrasinda yeni bir array olusturup return ediyoruz.
    hazirla() {
        let prm;
        let buffer;
        let base64;
        let obj = {}
        this.setState({ loading: true })
        this.setState({ errors: [] })
        this.setState({ pswrd: "" })

        // console.log("dosya sayisi"+this.state.files)
        prm = this.state.files.map((file, i) => {

            let x;

            if (file.name.endsWith(".docx") || file.name.endsWith(".xlsx") || file.name.endsWith(".pptx")) {
                base64 = this.base64Ayristir(file.src.base64) // base64 olarak gelen veri base64ayristir ile raw veriyi aliyoruz ve contenttype ayristiriliyor
                buffer = Buffer.from(base64.realData, 'base64') // dosya word ozelliklerini okumak için buffer yapıyoruz.

                x = belgeOzellikleri(buffer) // office document properties okuyan kutuphane
                    .then(i => {
                        return { belgeTipi1: i.title, guncellemeTarihi: i.modified }
                    })
            }

            let y = this.dosyaAdiAyristir(file.name)
            let z = Promise.resolve({ id: i, lastModified: file.src.file.lastModified, dosya: file.src.base64 }) // dosyayi ve son guncelleme tarihini ms cinsinden aliyoruz

            return Promise.all([x, y, z]).then(i => {

                obj = { PID: "", projeAdi: "", projeKisaAdi: "", projeUzunAdi: "", isTipi: "", yasatan: "", versiyon: "", kaynak: "", yonetici: "", belgeTipi: "", yayinDurumu: "Gönder", silDurumu: "İptal" }

                if (i[1] !== false) {
                    i.forEach(a => Object.assign(obj, a))
                    return obj
                } else return false

            })
        })

        Promise.all(prm).then(x => {
            x = x.filter(a => a !== false)
  
  /*          // mevcut dosyalarin uzerine ekleme yapabilmek icin, eski ekrandaki proje dosyaları silinmesin diye
            let yeniD = this.state.yeniDosya
            x.forEach(i=>{
                yeniD.push(x[i])
            })
            
            console.log(x)
*/
            // state degisikligi yapiyoruz
            this.setState({ yeniDosya:x })

            // sunucuya PID ve Belgetipi'ni soran fonksiyon ve cevabi setstate ile yenidosya olarak yaziyor.
            this.sunucuSorgula()
        })
    }

    // admin butonuna tiklandiginda sunucudan tum upload edilmis proje dosyaları listeleniyor.
    // state uzerindeki psswrd uzerine de password yaziliyor.
    handleAdminDosyalari() {
        this.toggle2()

        let ClientID = ""
        let adminPassword = document.getElementById("password").value


        fetch(host + 'projeler', {
            method: 'options',
            body: JSON.stringify({ ClientID, adminPassword }),
            headers: {
                'Content-Type': 'application/json',
            }
        })
            .then(x => {
                this.setState({ kullanici: x.headers.get("kullanici") }) // cevap admin olarak dondugunden, AdminTable componenti yukleniyor.
                return x.json()
            })
            .catch(error => console.error("hata:" + error))
            .then(yeniDosya => {
                //   console.log("yeni admin: "+ JSON.stringify(yeniDosya))

                if (Object.keys(yeniDosya).length > 0) {
                    this.setState({ yeniDosya, stateDegisti:true })
                    this.setState({ pswrd: adminPassword })
                }

            })
    }

    componentWillMount() {
        console.clear();

        this.clientDosyalari() // kullanici sayfaya ilk girdiginde, ClientID ye eslesen projeler otomatik olarak önüne düsüyor.
    }


    render() {

        const { toggle } = this.state
        const { toggle2 } = this.state

      //   console.log(this.state.kullanici)

        return <Files
            multiple={true}
            maxSize="5mb"
            multipleMaxSize="100mb"
            accept={["image/jpeg", "image/png", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"]}
            multipleMaxCount={500}
            convertToBase64
            onSuccess={files => { this.setState({ files: files }); this.hazirla() }}
            onError={hatalar => { this.setState({ errors: hatalar.length > 0 ? hatalar.map(i => i.file.name) : [] }); this.setState({ yeniDosya: [] }) }}
        >
            {({ browseFiles, getDropZoneProps }) => {
                return (
                    <div>
                        <div
                            {...getDropZoneProps({
                                style: {
                                    width: "100%",
                                    height: "100%",
                                    position: "absolute",
                                    border: "0px lightgray solid",
                                    padding: "10px"
                                }
                            })}
                        >
                            <label>Lütfen dosyaları buraya sürükleyin veya</label>
                            <div>
                                Dosyaları yüklemek için
                                <button onClick={browseFiles} >buraya</button> tıklayın.
                                Admin olmak için:
                                <button onClick={this.toggle2} > Admin </button> tıklayın.
                        </div>
                            <div>
                                <Drawer open={toggle} direction='bottom' onRequestClose={this.toggle} containerElementClass='drawer'>
                                    <div>
                                        Etiket sayısı 7 olmalı. Ya da sadece proje id numarası ve belge türü şeklinde upload edebilirsiniz. <br />
                                        <br /> Örnek:
                                    <ol>
                                            <li> 658.docx </li>
                                            <li> 777_Proje Tanım Belgesi.docx </li>
                                            <li> 10206_Proje Performans Belgesi9.docx </li>
                                            <li><b>10206_AYDOS (Uzun Proje Adı)_Proje Paydaş Belgesi.docx </b></li>
                                            <li> 382_ALO (Tesekkür Belgesi)_Yaz Gel_EDB_v1.0_Dışkaynak_PMO_Proje Tanım Belgesi.docx </li>
                                            <li> AYDOS Projesi.docx </li>
                                            <li> ALO (Tesekkür Belgesi)_Yaz Gel_EDB_v1.0_Dışkaynak_Hakan ÖZDAŞÇI_Proje Tanım Belgesi.docx </li>
                                        </ol>
                                    </div>
                                    <button onClick={this.toggle} class="drawer-kapat" > kapat </button>
                                </Drawer>

                                <Drawer open={toggle2} direction='bottom' onRequestClose={this.toggle2} containerElementClass='drawer'>
                                    <div>

                                        <label> Şifre: </label>     <input type="password" id="password" onFous={this.handleAdminDosyalari} autoFocus />

                                    </div>
                                    <button onClick={this.toggle2} class="drawer-kapat" > İptal </button>
                                    <button onClick={this.handleAdminDosyalari} class="drawer-kapat" > Login </button>
                                </Drawer>

                                <button onClick={this.toggle} class="drawer-ac" > Örnekler </button>
                            </div>

                            <ol>
                                {this.state.errors.map(error => (
                                    <li className="hata" key={error.id}>
                                        Hatalı Dosya Adı: {JSON.stringify(error)}
                                    </li>
                                ))}


                            </ol>

                            { this.state.loading ? 
                            <div className='sweet-loading'>
                                <DotLoader
                                    css={override}
                                    sizeUnit={"px"}
                                    size={80}
                                    color={'#123abc'}
                                    loading={true}
                                />
                            </div> 
                            :
                            this.state.kullanici == "Misafir" ? <MisafirTable host={host} veriler={this.state} handleSubmit={this.handleSubmit} handleSil={this.handleSil} /> : <AdminTable host={host} veriler={this.state} handleSubmit={this.handleSubmit} handleSil={this.handleSil} handleRevizyon={this.handleRevizyon} />

                            }
                        </div>
                    </div>
                );
            }}
        </Files>

    }
}


export default App;



